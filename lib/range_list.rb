class RangeList
  attr_reader :list, :border_range

  def initialize
    @list = []
    @border_range = nil
  end

  # 1. first test can only append to self
  # 2. if can't, then find index that need merge
  # 3. merge that;
  def add(range)
    range_s, range_e = * range
    if validate_range_params(range_s, range_e)
      add_inner(ExcludeEndRange.new(range_s, range_e))
    end
  end

  def remove(range)
    range_s, range_e = * range
    if validate_range_params(range_s, range_e)
      remove_inner(ExcludeEndRange.new(range_s, range_e))
    end
  end

  def to_s
    list.map(&:print).join(" ")
  end

  def print
    p to_s
  end

  def inspect
    print
    p "board: #{border_range && border_range.print}"
  end

  private

  # remove ranges = range_e for handle this no change to our list
  def validate_range_params(range_s, range_e)
    range_s && range_e && range_s < range_e
  end

  def add_inner(range)
    if border_range
      if border_range.mixed?(range)
        add_to_list_inner(range)
      else
        if border_range.place_left?(range)
          list.unshift(range)
        else
          list.push(range)
        end
      end
      border_range.merge!(range)
    else
      list.push(range)
      @border_range = ExcludeEndRange.new(range.min, range.max)
    end
  end

  def add_to_list_inner(range)
    if range.contain?(border_range)
      list.clear
      list.push(range)
      @border_range = range
    else
      need_merge = []
      bigger_index = nil

      # this can be optimalize, speed up this lookup
      # use tree, skiplist or anyothers
      list.each_with_index do |item, index|
        if item.mixed?(range)
          need_merge.push(index)
        elsif item.place_left?(range)
          bigger_index = index
          break
        end
      end
      bigger_index ||= list.length

      need_merge.each { |index| range.merge!(list[index]) }
      list.insert(bigger_index, range)
      need_merge.reverse.each {|index| list.delete_at(index)}
    end
  end


  def remove_inner(range)
    if border_range && border_range.mixed?(range)
      remove_list_inner(range)
    end
  end

  def remove_list_inner(range)
    if range.contain?(border_range)
      list.clear
      @border_range = nil
    else
      need_remove = []
      list.each_with_index do |item, index|
        if item.mixed?(range)
          need_remove.push(index)
        elsif item.place_left?(range)
          break
        end
      end

      unless need_remove.empty?

        new_range_after_removed = []
        need_remove.each do |index|
          res = list[index].remove(range)
          new_range_after_removed.concat(res)
        end

        insert_index = need_remove[0]
        need_remove.reverse.each {|index| list.delete_at(index)}
        new_range_after_removed.each_with_index {|item, index| list.insert(insert_index + index, item)}
        @border_range = list_max_range
      end
    end
  end

  def list_max_range
    case list.size
    when 0
      nil
    when 1
      ExcludeEndRange.new(list[0].min, list[0].max)
    else
      ExcludeEndRange.new(list[0].min, list[-1].max)
    end
  end

end

