MAX_LEVEL = 5
class SkipListNode
  attr_accessor :back, :range, :levels, :level

  def initialize(back, range, level)
    @back = back
    @range = range
    @levels = Array.new(MAX_LEVEL)
    @level = level
  end

  def to_s
    res = levels[0...level].map do |item|
      if item
        item.range.print
      else
        "nil"
      end
    end.join(",")
    "range: #{range ? range.print : 'nil'}, levels: #{res}"
  end

  def forward
    levels[0]
  end

  def set_level(level)
    @level = level > @level ? level : @level
  end

  def levels_used
    @levels[0...level]
  end

end

class SkipList
  attr_accessor :header, :tail, :length, :max_level

  def initialize
    @max_level = 1
    @header = SkipListNode.new(nil, nil, 1)
    @tail = nil
  end

  def insert(range)
    i_level = max_level
    node = header
    updates = Array.new(MAX_LEVEL)
    i_level -= 1

    # p "find target action"
    while i_level >= 0 do
      while node.levels[i_level] && range.place_left?(node.levels[i_level].range) do
        node = node.levels[i_level]
      end
      # p "node is: #{node.range.send(:print)}, range: #{range.send(:print)} : ilevel: #{i_level}"
      updates[i_level] = node
      i_level -= 1
    end

    # node alway is at  range left
    # p "find node: #{node.to_s}, insert range: #{range.print}"

    walker = node.forward
    need_merge = []
    while walker && walker.range && walker.range.mixed?(range)
      need_merge.push(walker)
      walker = walker.forward
    end

    if  need_merge.empty?
      n_level = random_level
      n_node  = SkipListNode.new(nil, range, n_level)
      # p "range: #{range.print}, n_level: #{n_level}"

      if n_level > max_level
        (max_level...n_level).each {|i| updates[i] = header}
        @max_level = n_level
      end

      # p "updates: #{updates.map{|item|item.to_s}}"
      (0...n_level).each {|i| updates[i].set_level(n_level) }
      (0...n_level).each do |i|
        n_node.levels[i] = updates[i].levels[i]
        updates[i].levels[i] = n_node
      end

      n_node.back = updates[0] == header ? nil : updates[0]
      if n_node.levels[0]
        n_node.levels[0].back= n_node
      else
        @tail = n_node
      end
    else
      need_merge.each {|item| item.levels}
      remove_updates = Array.new(MAX_LEVEL)
      need_merge[1..-1].each do |item|
        item.levels_used.each_with_index do |n_node, i|
          remove_updates[i] = n_node
        end
        range.merge!(item.range)
      end

      node = need_merge[0]
      node.range = range

      (node.level...max_level).each { |i|  updates[i].levels[i] = remove_updates[i] }
      (0..node.level).each {|i| node.levels[i] = remove_updates[i] }
      remove_updates[0] && remove_updates[0].back = node
    end
  end

  def random_level
    # @time ||= 4
    rand(MAX_LEVEL) + 1
  end

  def print
    walker = header
    while walker do
      p walker.to_s
      # binding.pry
      walker = walker.forward
    end
  end

end


class SkipRangeList
  attr_reader :list, :border_range

  def initialize
    @list = SkipList.new()
    @border_range = nil
  end

  # 1. first test can only append to self
  # 2. if can't, then find index that need merge
  # 3. merge that;
  def add(range)
    range_s, range_e = * range
    if validate_range_params(range_s, range_e)
      add_inner(ExcludeEndRange.new(range_s, range_e))
    end
  end

  def remove(range)
    range_s, range_e = * range
    if validate_range_params(range_s, range_e)
      remove_inner(ExcludeEndRange.new(range_s, range_e))
    end
  end

  def print
    list.print
  end

  # remove ranges = range_e for handle this no change to our list
  def validate_range_params(range_s, range_e)
    range_s && range_e && range_s < range_e
  end

  def add_inner(range)
    if border_range
      list.insert(range)
      border_range.merge!(range)
    else
      list.insert(range)
      @border_range = ExcludeEndRange.new(range.min, range.max)
    end
  end

  # def add_to_list_inner(range)
  #   if range.contain?(border_range)
  #     list.clear
  #     list.push(range)
  #     @border_range = range
  #   else
  #     need_merge = []
  #     bigger_index = nil

  #     list.each_with_index do |item, index|
  #       if item.mixed?(range)
  #         need_merge.push(index)
  #       elsif item.place_left?(range)
  #         bigger_index = index
  #         break
  #       end
  #     end
  #     bigger_index ||= list.length

  #     need_merge.each { |index| range.merge!(list[index]) }
  #     list.insert(bigger_index, range)
  #     need_merge.reverse.each {|index| list.delete_at(index)}
  #   end
  # end

end

