class ExcludeEndRange
  attr_accessor :min, :max

  def initialize(rs, re)
    @min, @max  = rs, re
  end

  def mixed?(other)
    !(min > other.max || max < other.min)
  end

  def merge!(other)
    @min = @min < other.min ? @min : other.min
    @max = @max > other.max ? @max : other.max
    self
  end

  def remove(other)
    if other.contain?(self)
      return []
    end
    if self.contain?(other)
      res = []
      if min != other.min
        res.push(ExcludeEndRange.new(min, other.min))
      end
      if max != other.max
        res.push(ExcludeEndRange.new(other.max, max))
      end
      res
    else
      if other.max <= max
        if other.max == max
          []
        else
          [ExcludeEndRange.new(other.max, max)]
        end
      else
        if min == other.min
          []
        else
          [ExcludeEndRange.new(min, other.min)]
        end
      end
    end
  end

  def place_left?(other)
    other.max < min
  end

  def contain?(other)
    @min <= other.min && @max >= other.max
  end

  def print
    "[#{min}, #{max})"
  end

end
