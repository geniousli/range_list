require 'rubygems'
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __FILE__)
require 'bundler/setup'
require 'pry'
lib = File.expand_path('../lib', __FILE__)
Dir["#{lib}/*"].each {|item| require item}
