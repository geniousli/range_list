require_relative '../boot.rb'
require 'minitest'
require 'minitest/autorun'



class ExcludeEndRangeTest < Minitest::Test
  attr_accessor :range10_20,:range8_9,:range8_10,:range8_11,:range11_15,:range18_30,:range20_21,:range21_30, :range8_30, :range10_10, :range20_20

  def initialize(_)
    super
    @range10_20 = ExcludeEndRange.new(10, 20)
    @range8_9 = ExcludeEndRange.new(8, 9)
    @range8_10 = ExcludeEndRange.new(8, 10)
    @range10_10 = ExcludeEndRange.new(10, 10)
    @range8_11 = ExcludeEndRange.new(8, 11)
    @range11_15 = ExcludeEndRange.new(11, 15)
    @range18_30 = ExcludeEndRange.new(18, 30)
    @range20_20 = ExcludeEndRange.new(20, 20)
    @range20_21 = ExcludeEndRange.new(20, 21)
    @range21_30 = ExcludeEndRange.new(21, 30)
    @range8_30 = ExcludeEndRange.new(8, 30)
  end

  def test_mixed
    assert range10_20.mixed?(range8_9) == false
    assert range10_20.mixed?(range8_10) == true
    assert range10_20.mixed?(range8_11) == true

    assert range10_20.mixed?(range11_15) == true

    assert range10_20.mixed?(range18_30) == true

    assert range10_20.mixed?(range20_21) == true

    assert range10_20.mixed?(range21_30) == false
    # two border case we set it to be trueth
    assert range10_20.mixed?(range20_20) == true
    assert range10_20.mixed?(range10_10) == true
  end

  def test_contain
    assert range10_20.contain?(range8_9) == false
    assert range10_20.contain?(range8_10) == false
    assert range10_20.contain?(range8_11) == false

    assert range10_20.contain?(range11_15) == true

    assert range10_20.contain?(range18_30) == false

    assert range10_20.contain?(range20_21) == false

    assert range10_20.contain?(range21_30) == false


    assert range10_20.contain?(range10_10) == true
    assert range10_20.contain?(range20_20) == true
  end


  def test_place_left
    assert range10_20.place_left?(range8_9) == true
    assert range10_20.place_left?(range8_11) == false

    assert range10_20.place_left?(range11_15) == false

    assert range10_20.place_left?(range18_30) == false

    assert range10_20.place_left?(range20_21) == false

    assert range10_20.place_left?(range21_30) == false
  end


  # only merge can mixed range
  def test_merge
    irange = range10_20.dup

    irange.merge!(range8_10)
    assert irange.min == 8
    assert irange.max == 20

    assert range10_20.min == 10
    assert range10_20.max == 20


    irange = range10_20.dup
    irange.merge!(range11_15)
    assert irange.min == 10
    assert irange.max == 20


    irange = range10_20.dup
    irange.merge!(range18_30)
    assert irange.min == 10
    assert irange.max == 30

    irange = range10_20.dup
    irange.merge!(range20_21)
    assert irange.min == 10
    assert irange.max == 21

    # border case
    irange = range10_20.dup
    irange.merge!(range20_20)
    assert irange.min == 10
    assert irange.max == 20

    # border case
    irange = range10_20.dup
    irange.merge!(range10_10)
    assert irange.min == 10
    assert irange.max == 20

  end

  # only test remove mixed range
  def test_remove
    irange = range10_20.dup
    vals = irange.remove(range8_30)
    assert vals.empty?

    irange = range10_20.dup
    vals = irange.remove(ExcludeEndRange.new(10, 11))
    assert vals[0].min == 11
    assert vals[0].max == 20

    irange = range10_20.dup
    vals = irange.remove(range11_15)
    assert vals.size == 2
    assert vals[0].max == 11
    assert vals[0].min == 10

    assert vals[1].min == 15
    assert vals[1].max == 20



    irange = range10_20.dup
    vals = irange.remove(range8_11)
    assert vals.size == 1
    assert vals[0].max == 20
    assert vals[0].min == 11


    irange = range10_20.dup
    vals = irange.remove(range18_30)
    assert vals.size == 1
    assert vals[0].max == 18
    assert vals[0].min == 10

    # border case
    irange = range10_20.dup
    vals = irange.remove(range10_10)
    assert vals[0].min == 10

    irange = range10_20.dup
    vals = irange.remove(range20_20)
    assert vals[0].max == 20
  end

end
