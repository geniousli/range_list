require_relative '../boot.rb'
require 'minitest'


require 'minitest/autorun'



class RangeListTest < Minitest::Test


  def test_add
    list = RangeList.new()
    list.add([2, 5])
    assert list.to_s == "[2, 5)"

    list.add([10, 20])
    assert list.to_s == "[2, 5) [10, 20)"

    list.add([6, 8])
    assert list.to_s == "[2, 5) [6, 8) [10, 20)"


    list.add([0, 1])
    assert list.to_s == "[0, 1) [2, 5) [6, 8) [10, 20)"

    # add border
    list.add([20, 20])
    assert list.to_s == "[0, 1) [2, 5) [6, 8) [10, 20)"

    list.add([5, 5])
    assert list.to_s == "[0, 1) [2, 5) [6, 8) [10, 20)"

    # add border
    list.add([20, 21])
    assert list.to_s == "[0, 1) [2, 5) [6, 8) [10, 21)"

    # merge one
    list.add([1, 2])
    assert list.to_s == "[0, 5) [6, 8) [10, 21)"

    # merge two
    list.add([1, 7])
    assert list.to_s == "[0, 8) [10, 21)"


    list.add([ 25, 30])
    assert list.to_s == "[0, 8) [10, 21) [25, 30)"

    list.add([0, 31])
    assert list.to_s == "[0, 31)"
  end


  def test_remove
    list = RangeList.new()

    list.remove([1, 100])

    list.add([1, 20])
    assert list.to_s == "[1, 20)"

    list.remove([0, 100])
    assert list.to_s == ""

    list.add([1, 10])

    list.add([15, 20])
    assert list.to_s == "[1, 10) [15, 20)"

    # remove border
    list.remove([1, 2])
    assert list.to_s == "[2, 10) [15, 20)"


    list.remove([15, 15])
    assert list.to_s == "[2, 10) [15, 20)"


    # remove outside
    list.remove([20, 100])
    assert list.to_s == "[2, 10) [15, 20)"

    list.remove([0, 2])
    assert list.to_s == "[2, 10) [15, 20)"

    # remove outside with border
    list.remove([0, 3])
    assert list.to_s == "[3, 10) [15, 20)"

    list.remove([19, 100])
    assert list.to_s == "[3, 10) [15, 19)"

    # remove inside
    list.remove([4, 8])
    assert list.to_s == "[3, 4) [8, 10) [15, 19)"


    # remove one range
    list.remove([7, 10])
    assert list.to_s == "[3, 4) [15, 19)"


    list.add([8, 10])
    assert list.to_s == "[3, 4) [8, 10) [15, 19)"


    # two range remove
    list.remove([4, 16])
    assert list.to_s == "[3, 4) [16, 19)"


    list.remove([2, 18])
    assert list.to_s == "[18, 19)"

    list.remove([18, 19])
    assert list.to_s == ""
  end
end
